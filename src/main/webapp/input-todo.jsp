<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="css/style-input-to-do.css">
</head>
<body>
<body>
  <form action='/WebClient/input-todo' method='POST'>
 <div class="header">
   <p>Enter information about task: </p>
</div>
<div class="form">
<div class="main">
<div class="field">
 <p><label>Firstname</label><input type="text" required name="firstname"/></p>
</div>
<div class="field">
   <p><label>Secondname</label><input type="text" required name="secondname"/></p>
</div>
<div class="field">
   <p><label>Patronymic</label><input type="text" required name="patronymic"/></p>
</div>
<div class="field">
 <p><label>Date of finish</label>
<input type="date" required name="finish" style="width:169px;"></p>
</div>
<p><label>Task text</label><textarea required name="message"></textarea></p>
<div class="button">
   <p><input type="submit" value="Send"></p>
</div>
</div>
</div>
  </form>
</body>
</body>
</html>