<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored = "false" %>
<%@ page import = "domain.ToDo,java.util.*" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>To-do</title>
	<link rel="stylesheet" type="text/css" href="css/style-result.css">
</head>
<body>
<table>
<tr>
    <th>Name</th>
    <th>Date</th>
    <th>Text</th>
</tr>
<form action="/WebClient/menu" method='POST'>
<c:forEach items="${mapToDo}" var="entry">
<tr>
<td><button type="submit" name='todo' value='${entry.key}1'>${entry.value.getPerson()}</button></td>
<td><button type="submit" name='todo' value='${entry.key}2'>${entry.value.getDate()}</button></td>
<td><button type="submit" name='todo' value='${entry.key}3'>${entry.value.getMessage()}</button></td>
<td><button type="submit" name='todo' value='${entry.key}4'>&times;</button></td>
<tr>
</c:forEach>
</table>
<div class="field">
<button type="submit" name='todo' value='10'>Add task</button>
</div>
</form>
</body>
</html>