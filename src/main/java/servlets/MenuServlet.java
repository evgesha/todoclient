package servlets;

import domain.ToDo;
import helpClass.SaveData;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MenuServlet extends SaveData {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        int numberWay = Integer.parseInt(request.getParameter("todo"));
        request.getSession().setAttribute("numberWay", Integer.toString(numberWay / 10));
        try {
            switch (numberWay % 10) {
                case 0:
                    request.getRequestDispatcher("input-todo.jsp").forward(request, response);
                    break;
                case 1:
                    request.getRequestDispatcher("input-person.jsp").forward(request, response);
                    break;
                case 2:
                    request.getRequestDispatcher("input-date.jsp").forward(request, response);
                    break;
                case 3:
                    request.getRequestDispatcher("input-text.jsp").forward(request, response);
                    break;
                case 4:
                    webClient.questionRemoveToDo(numberWay / 10);
                    Map<Integer, ToDo> toDoMap = new HashMap<>(webClient.questionGetToDoList());
                    request.getSession().setAttribute("mapToDo", toDoMap);
                    request.getRequestDispatcher("menu.jsp").forward(request, response);
                    break;
            }
        } catch (IOException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        } catch (ServletException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        }
    }

}
