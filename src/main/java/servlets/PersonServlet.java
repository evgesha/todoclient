package servlets;


import domain.Person;
import domain.ToDo;
import helpClass.SaveData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PersonServlet extends SaveData {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String firstname = request.getParameter("firstname");
        String secondname = request.getParameter("secondname");
        String patronymic = request.getParameter("patronymic");
        String number = (String) request.getSession().getAttribute("numberWay");
        Person person = new Person(firstname, secondname, patronymic);
        try {
            webClient.questionRefreshToDoPerson(Integer.parseInt(number), person);
            Map<Integer, ToDo> toDoMap = new HashMap<>(webClient.questionGetToDoList());
            request.getSession().setAttribute("mapToDo", toDoMap);
            request.getRequestDispatcher("menu.jsp").forward(request, response);
        } catch (IOException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        } catch (ServletException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        }
    }
}
