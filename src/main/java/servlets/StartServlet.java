package servlets;

import domain.ToDo;
import helpClass.SaveData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class StartServlet extends SaveData {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            Map<Integer, ToDo> toDoMap = new HashMap<>(webClient.questionGetToDoList());
            request.getSession().setAttribute("mapToDo", toDoMap);
            request.getRequestDispatcher("menu.jsp").forward(request, response);
        } catch (IOException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        } catch (ServletException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        }
    }
}

//<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
