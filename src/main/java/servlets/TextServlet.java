package servlets;

import domain.ToDo;
import helpClass.SaveData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TextServlet extends SaveData {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String number = (String) request.getSession().getAttribute("numberWay");
        String text = request.getParameter("message");
        try {
            webClient.questionRefreshToDoMessage(Integer.parseInt(number), text);
            Map<Integer, ToDo> toDoMap = new HashMap<>(webClient.questionGetToDoList());
            request.getSession().setAttribute("mapToDo", toDoMap);
            request.getRequestDispatcher("menu.jsp").forward(request, response);
        } catch (IOException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        } catch (ServletException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        }
    }
}
