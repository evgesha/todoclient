package servlets;

import domain.ToDo;
import helpClass.SaveData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class DateServlet extends SaveData {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String date = request.getParameter("calendar");
        LocalDate calendar = LocalDate.parse(date);
        String number = (String) request.getSession().getAttribute("numberWay");
        try {
            webClient.questionRefreshToDoDate(Integer.parseInt(number), calendar);
            Map<Integer, ToDo> toDoMap = new HashMap<>(webClient.questionGetToDoList());
            request.getSession().setAttribute("mapToDo", toDoMap);
            request.getRequestDispatcher("menu.jsp").forward(request, response);
        } catch (IOException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        } catch (ServletException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        }
    }
}
