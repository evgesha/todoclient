package servlets;


import domain.Person;
import domain.ToDo;
import helpClass.SaveData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class TaskServlet extends SaveData {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String firstname = request.getParameter("firstname");
        String secondname = request.getParameter("secondname");
        String patronymic = request.getParameter("patronymic");
        String date = request.getParameter("finish");
        String text = request.getParameter("message");
        ToDo toDo = new ToDo(LocalDate.parse(date), text, new Person(firstname, secondname, patronymic));
        try {
            webClient.questionAddToDo(toDo);
            Map<Integer, ToDo> toDoMap = new HashMap<>(webClient.questionGetToDoList());
            request.getSession().setAttribute("mapToDo", toDoMap);
            request.getRequestDispatcher("menu.jsp").forward(request, response);
        } catch (IOException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        } catch (ServletException error) {
            logger.info("ServletException : code = " + " - " + error.getMessage());
        }
    }
}
