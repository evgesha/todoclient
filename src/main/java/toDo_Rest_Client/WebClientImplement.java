package toDo_Rest_Client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import domain.Person;
import domain.ToDo;
import json.JSON;
import pojo.AnyDate;
import pojo.ToDoOfDate;
import pojo.ToDoOfMessage;
import pojo.ToDoOfPerson;

import javax.ws.rs.core.UriBuilder;
import java.time.LocalDate;
import java.util.Map;
import java.util.logging.Logger;

public class WebClientImplement implements WebClient {
    private JSON json;
    private Client client;
    private WebResource service;
    protected static Logger logger = Logger.getLogger(WebClientImplement.class.getName());

    public WebClientImplement() {
        json = new JSON();
        ClientConfig config = new DefaultClientConfig();
        client = Client.create(config);
        //service = client.resource(UriBuilder.fromUri("http://localhost:8000/ToDo/webapi/to-do").build());
        service = client.resource(UriBuilder.fromUri("http://localhost:8080/ToDo/webapi/to-do").build());
    }

    public Map<Integer, ToDo> questionGetToDoList() {
        String answer = service.path("all").path("task").header("Content-type", "application/json;charset=UTF-8").get(String.class);
        return json.jsonOutListToDo(answer);
    }

    public Map<Integer, ToDo> questionGetToDoOfDateBetween(LocalDate dateStart, LocalDate dateFinish) {
        AnyDate anyDate = new AnyDate(dateStart, dateFinish);
        String question = json.jsonInAnyDate(anyDate);
        ClientResponse response = service.path("task").path("date").path("between").header("Content-type", "application/json;charset=UTF-8").put(ClientResponse.class, question);
        String answer = response.getEntity(String.class);
        return json.jsonOutListToDo(answer);
    }

    public Map<Integer, ToDo> questionGetToDoOfPerson(Person person) {
        String question = json.jsonInPerson(person);
        ClientResponse response = service.path("task").path("person").header("Content-type", "application/json;charset=UTF-8").put(ClientResponse.class, question);
        String answer = response.getEntity(String.class);
        return json.jsonOutListToDo(answer);
    }

    public Map<Integer, ToDo> questionGetToDoOfDate(LocalDate date) {
        String question = json.jsonInDate(date);
        ClientResponse response = service.path("task").path("date").header("Content-type", "application/json;charset=UTF-8").put(ClientResponse.class, question);
        String answer = response.getEntity(String.class);
        return json.jsonOutListToDo(answer);
    }

    public boolean questionRefreshToDoMessage(int toDoId, String message) {
        ToDoOfMessage toDoOfMessage = new ToDoOfMessage(toDoId, message);
        String question = json.jsonInToDoOfMessage(toDoOfMessage);
        ClientResponse response = service.path("refresh").path("message").header("Content-type", "application/json;charset=UTF-8").put(ClientResponse.class, question);
        String answer = response.getEntity(String.class);
        return Boolean.valueOf(answer);
    }

    public boolean questionRefreshToDoDate(int toDoId, LocalDate date) {
        ToDoOfDate toDoOfDate = new ToDoOfDate(toDoId, date);
        String question = json.jsonInToDoOfDate(toDoOfDate);
        ClientResponse response = service.path("refresh").path("date").header("Content-type", "application/json;charset=UTF-8").put(ClientResponse.class, question);
        String answer = response.getEntity(String.class);
        return Boolean.valueOf(answer);
    }

    public boolean questionRefreshToDoPerson(int toDoId, Person person) {
        ToDoOfPerson toDoOfPerson = new ToDoOfPerson(toDoId, person);
        String question = json.jsonInToDoOfPerson(toDoOfPerson);
        ClientResponse response = service.path("refresh").path("person").header("Content-type", "application/json;charset=UTF-8").put(ClientResponse.class, question);
        String answer = response.getEntity(String.class);
        return Boolean.valueOf(answer);
    }

    public void questionRemoveToDoOfMessage(String message) {
        service.path("remove").path("message").header("Content-type", "application/json;charset=UTF-8").delete(message);
    }

    public void questionRemoveToDoOfPerson(Person person) {
        String question = json.jsonInPerson(person);
        service.path("remove").path("person").header("Content-type", "application/json;charset=UTF-8").delete(question);
    }

    public void questionRemoveToDoOfDate(LocalDate date) {
        String question = json.jsonInDate(date);
        service.path("remove").path("date").header("Content-type", "application/json;charset=UTF-8").delete(question);
    }

    public void questionRemoveToDo(int toDoId) {
        String question = Integer.toString(toDoId);
        service.path("remove").path("task").header("Content-type", "application/json;charset=UTF-8").delete(question);
    }

    public void questionAddToDo(ToDo toDo) {
        String question = json.jsonInToDo(toDo);
        service.path("add").path("task").header("Content-type", "application/json;charset=UTF-8").post(question);
    }
}
