package toDo_Rest_Client;

import domain.Person;
import domain.ToDo;

import java.time.LocalDate;
import java.util.Map;

public interface WebClient {
    Map<Integer, ToDo> questionGetToDoList();

    Map<Integer, ToDo> questionGetToDoOfDateBetween(LocalDate dateStart,LocalDate dateFinish);

    Map<Integer, ToDo> questionGetToDoOfPerson(Person person);

    Map<Integer, ToDo> questionGetToDoOfDate(LocalDate date);

    boolean questionRefreshToDoMessage(int toDoId, String message);

    boolean questionRefreshToDoDate(int toDoId, LocalDate date);

    boolean questionRefreshToDoPerson(int toDoId, Person person);

    void questionRemoveToDoOfMessage(String message);

    void questionRemoveToDoOfPerson(Person person);

    void questionRemoveToDoOfDate(LocalDate date);

    void questionRemoveToDo(int toDoId);

    void questionAddToDo(ToDo toDo);
}

